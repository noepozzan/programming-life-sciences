"""

"""

import os
import pathlib
import random
import string
from random import choices
import re
import numpy

import click


"""
run transcriptsampler on command line
"""


# helper function to create random words
def random_word(length):
    """
    """
    letters = string.ascii_lowercase
    actual_length = numpy.random.randint(low = 3, high = length, size = 1)[0]
    return ''.join(random.choice(letters) for i in range(actual_length))


# helper function
def dict_to_list(_dict):
    """
    """
    objects = []
    weights = []
    for _object, weight in sorted(_dict.items()):
        objects.append(_object)
        weights.append(weight)

    return objects, weights


# create a file with random gene names and random transcription numbers, since we don't have any actual input data
def create_gene_file(name, directory, number):
    file_name = os.path.join(directory, name)
    with open(file_name, "a") as file:
        for i in range(number):
            _string = "{} {} \n".format(random_word(6), numpy.random.randint(low = 10, high = 1000, size = 1)[0])
            file.write(_string)
        
    return file_name


# convert the file with gene names and transcription numbers into a dict
def read_avg_expression(file):
    dict_transcr = {}
    with open(file, "r") as dafile:
        data = dafile.readlines()
        for i in range(len(data)):
            data[i] = data[i].strip()
            number = int(re.findall(r'\d+', data[i])[0]) # goofy
            gene = data[i].split()[0]
            dict_transcr[gene] = number

    return dict_transcr


# probabilistically sample from the gene transcripts
def sample_transcripts(avgs, number):
    objects, weights = dict_to_list(avgs)
    sample_array = choices(population = objects, weights = weights, k = number)
    unique, counts = numpy.unique(sample_array, return_counts=True)
    new_dict = {key : value for key, value in zip(list(unique), list(counts))}
    return new_dict


@click.command()
@click.option("--input_file_name", default = "transcripts_numbers.txt")
@click.option("--sampled_file_name", default = "sampled_transcripts.txt")
@click.option("--number_genes", default = 20)
@click.option("--number_sampled", default = 1000)
@click.option("--directory", default = str(pathlib.Path().resolve()))
@click.option("--verbose", default = False)
def write_sample(input_file_name, sampled_file_name, number_genes, number_sampled, directory, verbose):
    """write the sampled dict into a file with chosen name

    Extended description of function.

    Args:
        input_file_name (str): name of the input file
        sampled_file_name (str): name of the output file
        number_genes (int): number of genes you want in your input file
        number_sampled (int): number of times you want to sample from your input file
        directory (str): directory where the produced files get placed
        verbose (bool): prints additional, informative output
        
    Returns:
        Returns the output file's name
    """    
    
    file_name = os.path.join(directory, sampled_file_name)
    file = create_gene_file(input_file_name, directory, number_genes)
    number_dict = read_avg_expression(file)
    sampled_dict = sample_transcripts(number_dict, number_sampled)
    
    with open(file_name, "w") as file:
        file.write(str(sampled_dict))
        
    if verbose:
        print("File {} and {} have been created in {}.".format(input_file_name,
                                                               sampled_file_name,
                                                               directory))
    return file_name
