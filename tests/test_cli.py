#import ast
from click.testing import CliRunner
import os
import pathlib
import pytest

from TranscriptSampler.cli import random_word, dict_to_list, create_gene_file, read_avg_expression, sample_transcripts, write_sample

#@pytest.mark.parametrize(
#    "test_input, exprected",
#    [(), "/Users/noepozzan/Documents/unibas/programming_life_sciences/tests/sampled_transcripts.txt"]
#)

#def test_write_sample(test_input, expected):
#    assert write_sample(test_input) == expected


def test_cli():
    # random_word
    assert isinstance(random_word(5), str) & len(random_word(5)) in range(0, 5)
    
    # dict_to_list
    assert isinstance(dict_to_list({1:2, 2:3}), tuple)
    assert isinstance(dict_to_list({1:2, 2:3})[1], list)
    
    # create_gene_file
    file = create_gene_file("file_name.txt",
                            pathlib.Path().resolve(),
                            20)
    assert os.path.isfile(file)
    
    # read_avg_expression
    _dict = read_avg_expression(file)
    assert isinstance(_dict, dict)
    
    # sample_transcripts
    assert isinstance(sample_transcripts(_dict, 1000), dict)
    
    # write_sample
    runner = CliRunner()
    result = runner.invoke(write_sample)
    file_name = "sampled_transcripts.txt"
    #assert result.output.strip() == os.path.join(pathlib.Path().resolve(), file_name)
    assert result.exit_code == 0
    #sample_file = write_sample("file_name.txt", "sample_file.txt", 20, 1000, pathlib.Path().resolve())
    #with open(sample_file, "r") as f:
    #    data = f.read()
    #asti = ast.literal_eval(data)
    #_sum = 0
    #for item, number in asti.items():
    #    _sum += number
    #assert os.path.isfile(sample_file)
    #assert _sum == 1000